<?php include 'inc/header.php';?>

<div class="container">
    <div class="header">
		<h2>Home Page</h2>
	</div>
	<div class="content">
            <div class="profile_info">
                <img src="https://bootdey.com/img/Content/avatar/avatar1.png"  >

                <div>
					<?php 
					if (isset($_SESSION['user'])) : ?>
                        <strong><?php echo $_SESSION['user']['username']; ?></strong>

                        <small>
                            <i  style="color: #888;">(<?php echo $_SESSION['user']['user_role']; ?>)</i>
                            <br>
                            <a href="logout.php?" style="color: red;">logout</a>
                        </small>
                       <?php  else:
                      echo "You Have to Login";
                    endif;
                    ?>
                </div>
            </div>
	</div>
</div>

<?php include 'inc/footer.php';?>