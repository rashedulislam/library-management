<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

    $book_name = $amount = $author_name = "";
    $errors = [];
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        if (isset($_POST['book_name']) && !empty($_POST['book_name'])) {
            $book_name = test_input($_POST['book_name']);
        } else {
            $errors['book_name_error'] = 'Please enter a valid Book name';
        }
        if (strlen($book_name) < 5) {
            $errors['book_len_error'] = "Choose a Bookname longer then 4 character";
        }

        if (isset($_POST['author_name']) && !empty($_POST['author_name'])) {
            $author_name = test_input($_POST['author_name']);
        } else {
            $errors['author_name_error'] = 'Please enter a valid Author name';
        }

        if (strlen($author_name) < 4) {
            $errors['author_len_error'] = "Author name short";
        }
    
        if (isset($_POST['amount']) && !empty($_POST['amount'])) {
            $amount = test_input($_POST['amount']);
        } else {
            $errors['amount_error'] = 'Please enter How many books present';
        }
        if ( $amount< 0) {
            $errors['book_number_error'] = "Book Amount can't be negative";
        }

    
        function BookExists($connection, $book_name)
        {
            $bookQuery = "SELECT * FROM books WHERE book_name=:book_name";
            $stmt = $connection->prepare($bookQuery);
            $stmt->execute([':book_name' => $book_name]);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    
        $bookexists = BookExists($connection, $book_name);
        if ($bookexists) {
            $errors['book_exist_error'] = "Sorry... Book already Exist";
        }

        if (empty($errors)) {
          $sql = 'INSERT INTO books( book_name, author_name, amount )
        VALUES(:book_name, :author_name, :amount )';
          $stmt = $connection->prepare($sql);
          if ($stmt->execute([':book_name' => $book_name, ':author_name' => $author_name, ':amount' => $amount ])) {
          header("Location: /library-management/admin-booklist.php");
          } else {
              echo 'Book add failed';
          }
      }
    }
    
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>

<div class="container">

<div class="signup-form">

    <form action="" method="post">
		<div class="form-header">
			<h2>Add a Book To The List</h2>
		</div>
    <div class="form-group">
      <label>Book Name</label>
      <input type="text" class="form-control" name="book_name" >

      <span class="text-danger">
<?php
if (isset($errors['book_name_error'])) {
    echo $errors['book_name_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['book_len_error'])) {
    echo $errors['book_len_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['book_exist_error'])) {
    echo $errors['book_exist_error'];
}
?></span>

    </div>

    <div class="form-group">
      <label>Author Name</label>
      <input type="text" class="form-control" name="author_name" >

<span class="text-danger">
<?php
if (isset($errors['author_name_error'])) {
    echo $errors['author_name_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['author_len_error'])) {
    echo $errors['author_len_error'];
}
?></span>
    </div>

    <div class="form-group">
      <label>Book Amount</label>
      <input type="text" class="form-control" name="amount" >
      <span class="text-danger">
<?php
if (isset($errors['amount_error'])) {
    echo $errors['amount_error'];
}
?></span>
<span class="text-danger">
<?php
if (isset($errors['book_number_error'])) {
    echo $errors['book_number_error'];
}
?></span>

    </div>
		<div class="form-group">
			<button name="submit" type="submit" class="btn btn-primary btn-block btn-lg">Add Book</button>
		</div>
    </form>
</div>
</div> <!-- /container -->

<?php include 'inc/footer.php';?>