<?php include 'inc/header.php';


if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}



$sql = 'SELECT * FROM users';
$stmt = $connection->prepare($sql);
$stmt->execute();
$users = $stmt->fetchAll(PDO::FETCH_OBJ);
?>


<div class="container">
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<div class="table-responsive">
				<table class="table user-list">
					<thead>
						<tr>
							<th><span>User</span></th>
							<th><span>Created</span></th>
							<th class="text-center"><span>Status</span></th>
							<th><span>Email</span></th>
							<th><span>Actions</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($users as $user) {?>

						<tr>
							<td>
								<img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="">
								<a href="view-user.php?user_id=<?php echo $user->user_id; ?>" class="user-link"><?php echo $user->username; ?></a>
								<span class="user-subhead"><?php echo $user->user_role; ?></span>
							</td>
							<td>
                            <?php echo $user->created_at; ?>
							</td>
							<td class="text-center">
								<span class="label label-default"><?php echo $user->user_role; ?></span>
							</td>
							<td>
                            <span><?php echo $user->user_email; ?></span>
							</td>
							<td style="width: 20%;">
								<a href="view-user.php?user_id=<?php echo $user->user_id; ?>" class="table-link">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="update-user.php?user_id=<?php echo $user->user_id; ?>" class="table-link">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="delete-user.php?user_id=<?php echo $user->user_id; ?>" class="table-link danger">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>

<?php include 'inc/footer.php';?>