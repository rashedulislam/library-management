<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$id = $_GET['id'];

$sql = 'SELECT * FROM book_user WHERE id = :id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':id' => $id]);
$request_book = $stmt->fetch(PDO::FETCH_OBJ);
$book_id = $request_book->book_id;


$sql = 'SELECT * FROM books WHERE book_id = :book_id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':book_id' => $book_id]);
$book = $stmt->fetch(PDO::FETCH_OBJ);

$current_amount = $book->amount;
$request_amount = $request_book->quantity;
$book_left = $current_amount - $request_amount;


$sql = 'UPDATE books SET amount = :book_left where book_id = :book_id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':book_left' => $book_left, 'book_id' => $book_id ]);

$sql = 'UPDATE book_user SET request_status = "approved", borrowed_at = now() where id = :id ';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':id' => $id])){
header("Location: /library-management/request.php");
}