<?php include 'inc/header.php';

if(isset($_SESSION['user'])){
$message_from = $_SESSION['user']['username'];
} else{
    header("location: /library-management/login.php");
}

$username = $message = "";
$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['username']) && !empty($_POST['username'])) {
        $username = test_input($_POST['username']);
    } else {
        $errors['username_error'] = 'Please enter a valid username';
    }

    function userExists($connection, $username)
    {
        $userQuery = "SELECT * FROM users WHERE username=:username";
        $stmt = $connection->prepare($userQuery);
        $stmt->execute(array(':username' => $username));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    $nameexists = userExists($connection, $username);
    if (!$nameexists) {
        $errors['no_username_error'] = "Sorry... member doesn't exist!";
    }

    if (isset($_POST['message']) && !empty($_POST['message'])) {
        $message = test_input($_POST['message']);
    } else {
        $errors['message_error'] = 'Message Field cannot be empty';
    }

    if (empty($errors)) {
        $sql = 'INSERT INTO messages(message_from, message_to, message)
			VALUES(:message_from, :username, :message)';
        $stmt = $connection->prepare($sql);
        if ($stmt->execute([':message_from' => $message_from, ':username' => $username, ':message' => $message])){
				header("Location: /library-management/messages.php");
        } else {
            echo 'Message send failed';
        }
    }

}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>


<form class="form-horizontal" action="" method="post">
  <div class="form-group">
    <label for="inputText3" class="col-sm-2 control-label">To</label>
    <div class="col-sm-6">
      <input name="username" type="text" class="form-control" id="inputText3" placeholder="username">
      <span class="text-danger">
<?php
if (isset($errors['username_error'])) {
    echo $errors['username_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['no_username_error'])) {
    echo $errors['no_username_error'];
}
?></span>

    </div>
  </div>
  <div class="form-group">
    <label for="message" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-6">
    <textarea name="message" class="form-control" rows="6"></textarea>

<span class="text-danger"><?php
if (isset($errors['message_error'])) {
    echo $errors['message_error'];
}
?></span>

    </div>
  </div>
 
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="submit" type="submit" class="btn btn-default">Send</button>
    </div>
  </div>
</form>


<?php include 'inc/footer.php';?>