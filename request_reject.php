<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$id = $_GET['id'];

$sql = 'UPDATE book_user SET request_status = "rejected", borrowed_at = now() where id = :id ';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':id' => $id])){
header("Location: /library-management/request.php");
}