<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$id = $_GET['id'];

$sql = 'SELECT * FROM book_user WHERE id = :id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':id' => $id]);
$return_book = $stmt->fetch(PDO::FETCH_OBJ);
$book_id = $return_book->book_id;


$sql = 'SELECT * FROM books WHERE book_id = :book_id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':book_id' => $book_id]);
$book = $stmt->fetch(PDO::FETCH_OBJ);

$previous_amount = $book->amount;
$return_amount = $return_book->quantity;
$present_book = $previous_amount + $return_amount;


$sql = 'UPDATE books SET amount = :present_book where book_id = :book_id ';
$stmt = $connection->prepare($sql);
$stmt->execute([':present_book' => $present_book, 'book_id' => $book_id ]);

$sql = 'UPDATE book_user SET request_status = "returned" where id = :id ';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':id' => $id])){
header("Location: /library-management/request.php");
}