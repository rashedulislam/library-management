<?php include 'inc/header.php';

if (!isset($_SESSION['user'])) {
    $_SESSION['msg'] = "You must log in as a user first";
	header("location: /library-management/login.php");
}

$message_id = $_GET['message_id'];

$sql = 'SELECT * FROM messages WHERE message_id = :message_id';
$stmt = $connection->prepare($sql);
$stmt->execute([':message_id' => $message_id]);
$message = $stmt->fetch(PDO::FETCH_OBJ);

$sql = 'UPDATE messages SET status = "read" where message_id = :message_id ';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':message_id' => $message_id]));

?>

<h1>Message Received From <?php echo $message->message_from ;?></h1>
<hr>
<h3>Message Body</h3>

<p><?php echo $message->message ;?></p>


<hr>
<a href="messages.php"><button type="button" class="btn btn-default">Return to inbox</button></a>



<?php include 'inc/footer.php';?>