<?php include 'inc/header.php';

$username = $user_email = $user_password = $confirm_password = $user_role = "";
$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['username']) && !empty($_POST['username'])) {
        $username = test_input($_POST['username']);
    } else {
        $errors['username_error'] = 'Please enter a valid username';
    }

    if (strlen($username) <= 3) {
        $errors['namelen_error'] = "Choose a username longer then 3 character";
    }

    $user_email = test_input($_POST['user_email']);
    if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $errors['email_error'] = 'Please enter a valid email address';
    }

    if (isset($_POST['user_password']) && !empty($_POST['user_password'])) {
        $user_password = $_POST["user_password"];
    } else {
        $errors['pass_error'] = 'Please enter your password';
    }

    if (isset($_POST['confirm_password']) && !empty($_POST['confirm_password'])) {
        $confirm_password = $_POST["confirm_password"];
    } else {
        $errors['repass_error'] = 'Please reenter your password';
    }

    if ($user_password != $confirm_password) {
        $errors['passmatch_error'] = "Password doesn't match";
    }

    if (strlen($user_password) <= 5) {
        $errors['passlen_error'] = "Choose a password longer then 5 character";
    }

    $user_role = "member";

    function userExists($connection, $username)
    {
        $userQuery = "SELECT * FROM users WHERE username=:username";
        $stmt = $connection->prepare($userQuery);
        $stmt->execute(array(':username' => $username));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    $nameexists = userExists($connection, $username);
    if ($nameexists) {
        $errors['nametaken_error'] = "Sorry... username already taken";
    }

    function emailExists($connection, $user_email)
    {
        $userQuery = "SELECT * FROM users WHERE user_email=:user_email";
        $stmt = $connection->prepare($userQuery);
        $stmt->execute(array(':user_email' => $user_email));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    $emailexists = emailExists($connection, $user_email);
    if ($emailexists) {
        $errors['emailtaken_error'] = "Sorry... Email already Exist";
    }

    if (empty($errors)) {
        $user_password = md5($user_password);
        $sql = 'INSERT INTO users(username, user_email, user_password, user_role)
			VALUES(:username, :user_email, :user_password, :user_role)';
        $stmt = $connection->prepare($sql);
        if ($stmt->execute([':username' => $username, ':user_email' => $user_email, ':user_password' => $user_password, ':user_role' => $user_role])) {
				header("Location: /library-management/login.php");
        } else {
            echo 'User Creation failed';
        }
    }

}

function getUserById($id){
    global $connection;
    $query = "SELECT * FROM users WHERE user_id=:id";
    $stmt = $connection->prepare($query);
    $stmt->execute([':id'=> $id]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    return $user;
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>



<div class="signup-form">
    <form action="" method="post">
		<div class="form-header">
			<h2>Sign Up Here</h2>
		</div>
        <div class="form-group">
			<label>Username</label>
        	<input type="text" class="form-control" name="username" >

<span class="text-danger">
<?php
if (isset($errors['namelen_error'])) {
    echo $errors['namelen_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['nametaken_error'])) {
    echo $errors['nametaken_error'];
}
?></span>

        </div>
        <div class="form-group">
			<label>Email Address</label>
        	<input type="email" class="form-control" name="user_email" >

<span class="text-danger">
<?php
if (isset($errors['email_error'])) {
    echo $errors['email_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['emailtaken_error'])) {
    echo $errors['emailtaken_error'];
}
?></span>

        </div>
		<div class="form-group">
			<label>Password</label>
            <input type="password" class="form-control" name="user_password" >

<span class="text-danger">
<?php
if (isset($errors['pass_error'])) {
    echo $errors['pass_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['passlen_error'])) {
    echo $errors['passlen_error'];
}
?></span>

        </div>
		<div class="form-group">
			<label>Confirm Password</label>
            <input type="password" class="form-control" name="confirm_password" >

<span class="text-danger">
<?php
if (isset($errors['repass_error'])) {
    echo $errors['repass_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['passmatch_error'])) {
    echo $errors['passmatch_error'];
}
?></span>

        </div>
		<div class="form-group">
			<button name="submit" type="submit" class="btn btn-primary btn-block btn-lg">Sign Up</button>
		</div>
    </form>

	<div class="text-center small">Already have an account? <a href="login.php">Login here</a></div>
</div>

<?php include 'inc/footer.php';?>