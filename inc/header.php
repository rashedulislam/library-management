<?php include 'db.php';

session_start(); 

function isAdmin()
	{
		if (isset($_SESSION['user']) && $_SESSION['user']['user_role'] == 'admin' ) {
			return true;
		}else{
			return false;
		}
  }

function isMember()
{
  if (isset($_SESSION['user']) && $_SESSION['user']['user_role'] == 'member' ) {
    return true;
  }else{
    return false;
  }
}

function isJailed()
{
  if (isset($_SESSION['user']) && $_SESSION['user']['user_role'] == 'jailed' ) {
    return true;
  }else{
    return false;
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Courgette|Pacifico:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="inc/style.css">
    <title>Library management</title>
</head>

<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/library-management/">Library Management</a>
    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php 
            if (isset($_SESSION['user']) && $_SESSION['user']['user_role']== 'admin') { ?>

              <li><a href="user-list.php">Users</a></li>
              <li><a href="addbooks.php">Add Books</a></li>
              <li><a href="admin-booklist.php">BookList</a></li>
              <li><a href="request.php">Requests</a></li>
              <li><a href="messages.php">Messages</a></li>
              <li><a href="create_message.php">Send Message</a></li>

              <?php  } elseif (isset($_SESSION['user']) && $_SESSION['user']['user_role']== 'member') { ?>

              <li><a href="userbooklist.php">BookList</a></li>
              <li><a href="borrowed_book.php">Borrowed Books</a></li>
              <li><a href="messages.php">Messages</a></li>
              <li><a href="create_message.php">Send Message</a></li>

              <?php } elseif (isset($_SESSION['user']) && $_SESSION['user']['user_role']== 'jailed') { ?>

              <li><a href="borrowed_book.php">Borrowed Books</a></li>
              <li><a href="messages.php">Messages</a></li>
              <li><a href="create_message.php">Send Message</a></li>
        
              <?php   } ?>

      </ul>

      <ul class="nav navbar-nav navbar-right">

       <?php 
       
       if(isset($_SESSION['user'])){ ?> 

        <li class="dropdown">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome <?php echo ($_SESSION['user']['username']);?><span class="caret"></span></a>

          <ul class="dropdown-menu">

            <li><a href="user-profile.php">Profile</a></li>

            <li><a href="update-user.php?user_id=<?php echo ($_SESSION['user']['user_id']);?>">Edit Profile</a></li>

            <li><a href="logout.php">Logout</a></li>

          </ul>
        </li>

       <?php } else { ?>

        <li class="nav-item">
           <a class="nav-link" href="login.php">Login</a>
        </li>
        <li class="nav-item">
           <a class="nav-link" href="register.php">Register</a>
        </li>

        <?php } ?>

      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
