<?php include 'inc/header.php';

if (!isMember()) {
	$_SESSION['msg'] = "You must log in as admin or member";
	header("location: /library-management/login.php");
}

$sql = 'SELECT * FROM books';
$stmt = $connection->prepare($sql);
$stmt->execute();
$books = $stmt->fetchAll(PDO::FETCH_OBJ);
?>

<div class="container">
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<div class="table-responsive">
				<table class="table user-list">
					<thead>
						<tr>
							<th><span>Book Name</span></th>
							<th><span>Author Name</span></th>
							<th><span>Availabe</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($books as $book) {?>

						<tr>
							<td>
								<a href="viewbook.php?book_id=<?php echo $book->book_id; ?>" class="user-link"><?php echo $book->book_name; ?></a>
							</td>
							<td>
                                <span><?php echo $book->author_name; ?></span>
							</td>
							<td>
                                <span><?php echo $book->amount; ?></span>
							</td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>

<?php include 'inc/footer.php';?>