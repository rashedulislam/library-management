<?php include 'inc/header.php';

if (!isset($_SESSION['user'])) {
    $_SESSION['msg'] = "You must log in as a user first";
	header("location: /library-management/login.php");
}

$book_id = $_GET['book_id'];

$sql = 'SELECT * FROM books WHERE book_id = :book_id';
$stmt = $connection->prepare($sql);
$stmt->execute([':book_id' => $book_id]);
$book = $stmt->fetch(PDO::FETCH_OBJ);

$present_amount = $book->amount;
$quantity = "";
$errors = [];


    if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        if (isset($_POST['quantity']) && !empty($_POST['quantity'])) {
            $quantity = test_input($_POST['quantity']);
        } else {
            $errors['quantity_error'] = 'Please enter a valid quantity';
        }
        if ( $quantity > $present_amount) {
            $errors['book_limit_error'] = "Not enough book";
        }
        if ( $quantity< 0) {
            $errors['book_number_error'] = "Book Amount can't be negative";
        }
        $user_id = $_SESSION['user']['user_id'];
  
        if (empty($errors)) {
         $sql = "INSERT INTO `book_user`( book_id, user_id, quantity)
                 VALUES (:book_id, :user_id, :quantity )";
         $stmt = $connection->prepare($sql);
         if ($stmt->execute([':book_id' => $book_id, ':user_id' => $user_id,':quantity' => $quantity ])) {
         header("Location: /library-management/userbooklist.php");
         } else {
             echo 'Book Request Failed';
         }
     }
}
        
function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<div class="container">

	<div class="content">

		<h2>Book Details </h2>

            <div class="profile_info">
                <img src="https://bootdey.com/img/Content/avatar/avatar2.png"  >

                <div>
                    <table class="view-user-table" >
                     <tr>
                        <th>Book Name</th>
                        <td><strong><?php echo $book->book_name; ?></strong></td>
                     </tr>
                     <tr>
                        <th>Author Name</th>
                        <td><strong><?php echo $book->author_name; ?></strong></td>
                     </tr>
                     <tr>
                        <th>Book Left</th>
                        <td><strong><?php echo $book->amount; ?></strong></td>
                     </tr>
                    </table>
                </div><br>

               <?php  if (isMember()) { ?>

                <form class="form-inline" action="" method="post">
                  <div class="form-group">
                     <label for="exampleInputName2">Book Amount</label>
                     <input type="text" class="form-control" id="exampleInputName2" placeholder="Book amount" name="quantity">

                     <span class="text-danger">
                     <?php
                     if (isset($errors['quantity_error'])) {
                        echo $errors['quantity_error'];
                     }
                     ?></span>
                     <span class="text-danger"><?php
                     if (isset($errors['book_limit_error'])) {
                        echo $errors['book_limit_error'];
                     }
                     ?></span>
                     <span class="text-danger"><?php
                     if (isset($errors['book_number_error'])) {
                        echo $errors['book_number_error'];
                     }
                     ?></span>
                  </div>
                  <button type="submit" name="submit" class="btn btn-default">Request</button>
                  </form>

                  <?php } ?>
            </div>
	</div>
</div>

<?php include 'inc/footer.php';?>