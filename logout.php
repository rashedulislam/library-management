<?php include 'inc/header.php';

	session_start();
	if(isset($_SESSION["user"])){
		unset($_SESSION["user"]);
	}
	session_destroy();
	session_write_close();

    header("location: /library-management/login.php");

