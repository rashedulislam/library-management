<?php include 'inc/header.php';

$user_id = $_SESSION['user']['user_id'];
$sql = 'SELECT * FROM users WHERE user_id = :user_id';
$stmt = $connection->prepare($sql);
$stmt->execute(['user_id' => $user_id]);
$user = $stmt->fetch(PDO::FETCH_OBJ);

if (isMember() or isJailed()) {
$user_id = $_SESSION['user']['user_id'];

$sql = 'SELECT MIN(borrowed_at) as date FROM book_user WHERE request_status="approved" && user_id = :user_id' ;
$stmt = $connection->prepare($sql);
$stmt->execute([':user_id' => $user_id]);
$date = $stmt->fetch(PDO::FETCH_OBJ);

$borrow_date = $date->date;
$borrow_date = strtotime($borrow_date);

if(empty($borrow_date)){
   $sql = 'UPDATE users SET user_role= "member" WHERE user_id = :user_id';
   $stmt = $connection->prepare($sql);
   $stmt->execute([':user_id' => $user_id]);
}

$today = date("Y-m-d");
$today = strtotime($borrow_date);

$diff = $borrow_date - $today ;
$week = 60*60*24*7;

if ($diff >= $week){
   $sql = 'UPDATE users SET user_role= "jailed" WHERE user_id = :user_id';
   $stmt = $connection->prepare($sql);
   $stmt->execute([':user_id' => $user_id]);
   }

}
?>



<div class="container">
    <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
          <h3>
             <?php 
                echo $_SESSION['success']; 
                unset($_SESSION['success']);
             ?>
          </h3>
      </div>
    <?php endif ?>
	<div class="content">
		<h2>Welcome to your profile <?php echo $user-> username ;?></h2>
        <div class="profile_info">
            <img src="https://bootdey.com/img/Content/avatar/avatar1.png"  >
        <div> <br>
            <table class="view-user-table" >
                <tr>
                <th>Username</th>
                <td><strong><?php echo $user->username; ?></strong></td>
                </tr>
                <tr>
                <th>Email Address</th>
                <td><strong><?php echo $user->user_email; ?></strong></td>
                </tr>
                <tr>
                <th>User Role</th>
                <td><strong><?php echo $user->user_role; ?></strong></td>
                </tr>
                <tr>
                <th>Register At</th>
                <td><strong><?php echo $user->created_at; ?></strong></td>
                </tr>
            </table>
        </div>
     </div>
  </div>
</div>

<?php include 'inc/footer.php';?>