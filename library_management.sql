-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 02, 2019 at 07:17 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`book_id`, `book_name`, `author_name`, `amount`) VALUES
(3, 'Sonar Tori', 'Robindranath', 9),
(6, 'Pather Panchali', 'Bibhutibhushan Bandyopadhyay', 7),
(7, 'Devdas', 'Sarat Chandra Chattopadhyay', 8),
(8, 'Chander Pahar', 'Shankar Roy Chowdhury', 4),
(9, 'Srikanta', 'Sarat Chandra Chattopadhyay.', 15),
(10, 'Padma Nadir Majhi', 'Manik Bandyopadhyay', 10);

-- --------------------------------------------------------

--
-- Table structure for table `book_user`
--

CREATE TABLE `book_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `request_status` varchar(50) NOT NULL DEFAULT 'pending',
  `borrowed_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_user`
--

INSERT INTO `book_user` (`id`, `book_id`, `user_id`, `quantity`, `request_status`, `borrowed_at`) VALUES
(1, 3, 28, 3, 'returned', '2019-11-01'),
(2, 6, 28, 5, 'rejected', '2019-11-01'),
(3, 3, 28, 5, 'returned', '2019-11-01'),
(4, 6, 28, 3, 'returned', '2019-11-02'),
(5, 9, 28, 6, 'returned', '2019-10-17'),
(6, 10, 28, 3, 'pending', '2019-11-02'),
(7, 9, 36, 5, 'returned', '2019-10-15'),
(8, 9, 34, 5, 'returned', '2019-10-17'),
(9, 9, 34, 4, 'returned', '2019-10-08'),
(10, 9, 36, 3, 'returned', '2019-11-03');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `message_from` varchar(255) NOT NULL,
  `message_to` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `sent_time` datetime NOT NULL DEFAULT current_timestamp(),
  `status` varchar(10) NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `message_from`, `message_to`, `message`, `sent_time`, `status`) VALUES
(1, 'rashedul', 'rashed', 'helo buddy', '2019-11-02 18:40:08', 'read'),
(2, 'rashed', 'rashedulislam', 'Hello Admin..Please remove me from jailed member list', '2019-11-02 19:08:18', 'read'),
(3, 'rashedulislam', 'rashedul', 'Have you done your work yet?', '2019-11-02 19:10:12', 'read'),
(4, 'rashedul', 'rashed', 'tatat tata taa tatat', '2019-11-02 21:53:15', 'read'),
(5, 'rashedul', 'rashedulislam', 'hi boss :P', '2019-11-02 21:53:34', 'unread'),
(6, 'Sobuj', 'rashedul', 'I am jailed :P', '2019-11-02 22:06:53', 'unread'),
(7, 'rashedul', 'Sobuj', 'hahahahahaahahahaha', '2019-11-02 22:12:48', 'unread');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` text NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_email`, `user_password`, `user_role`, `created_at`) VALUES
(20, 'rashedulislam', 'rashedulislam.ruet@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '2019-10-24 22:06:34'),
(28, 'rashedul', 'rashedul@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'member', '2019-10-25 14:57:15'),
(34, 'rashed', 'rashed@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'member', '2019-10-25 23:36:19'),
(36, 'Sobuj', 'sobuj@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'member', '2019-11-02 16:26:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`book_id`),
  ADD UNIQUE KEY `book_name` (`book_name`);

--
-- Indexes for table `book_user`
--
ALTER TABLE `book_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `book_user`
--
ALTER TABLE `book_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
