<?php include 'inc/header.php';

if(isset($_SESSION['user'])){
    $username = $_SESSION['user']['username'];
    } else{
        header("location: /library-management/login.php");
    }

$sql = 'SELECT * FROM messages WHERE message_to = :username && status="unread" ';
$stmt = $connection->prepare($sql);
$stmt->execute(['username' => $username]);
$received_messages = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql = 'SELECT * FROM messages WHERE message_from = :username ';
$stmt = $connection->prepare($sql);
$stmt->execute(['username' => $username]);
$sent_messages = $stmt->fetchAll(PDO::FETCH_OBJ);


?>

<div class="jumbotron">
<div class="table-responsive">
    <h2>Received Messages</h2>
    <table class="table">
        <thead>
            <tr>
                <th>Message From</th>
                <th>Message</th>
                <th>Date</th>
                <th>View Message</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($received_messages as $message){ ?>
            <tr>
                <td><?php echo $message->message_from; ?></td>
                <td><?php echo $message->message; ?></td>
                <td><?php echo $message->sent_time; ?></td>
                <td><a href="view_message.php?message_id=<?php echo $message->message_id?>"><button type="button" class="btn btn-default">View</button></a><td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <h2>Sent Messages</h2>
    <table class="table">
        <thead>
            <tr>
                <th>Message To</th>
                <th>Message</th>
                <th>Date</th>
                <th>Message Status</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($sent_messages as $message){ ?>
            <tr>
                <td><?php echo $message->message_to; ?></td>
                <td><?php echo $message->message; ?></td>
                <td><?php echo $message->sent_time; ?></td>
                <td><?php echo $message->status; ?><td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
</div>


<?php include 'inc/footer.php';?>