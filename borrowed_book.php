<?php include 'inc/header.php';

if (isset($_SESSION['user'])){
$user_id = $_SESSION['user']['user_id']; }
else{
	header("location: /library-management/login.php");
}

$sql = 'SELECT * FROM book_user INNER JOIN users ON book_user.user_id = users.user_id INNER JOIN books ON book_user.book_id = books.book_id WHERE book_user.user_id = :user_id && request_status = "approved" ';

$stmt = $connection->prepare($sql);
$stmt->execute([':user_id' => $user_id]);
$approved_books = $stmt->fetchAll(PDO::FETCH_OBJ);

?>

<div class="container">
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<div class="table-responsive">
				<table class="table user-list">
					<thead>
						<tr>
                            <th><span>Book Name</span></th>
                            <th><span>Author Name</span></th>
                            <th><span>Book Amount</span></th>
                            <th><span>Taken Date</span></th>
                            <th><span>Return Book</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($approved_books as $book) {?>

						<tr>
							<td>
								<h4><?php echo $book->book_name; ?></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->author_name; ?></span></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->quantity; ?></span></h4>
							</td>
                            <td>
                                <h4><span><?php echo $book->borrowed_at; ?></span></h4>
							</td>
                            <td>
                                <a href="return.php?id=<?php echo $book->id; ?>" class="table-link">
                                <span 
                                <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                </span>
                            </td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>

<?php include 'inc/footer.php';?>