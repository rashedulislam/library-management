<?php include 'inc/header.php';

if (isAdmin() or isMember()) {

$user_id = $_GET['user_id'];

$sql = 'SELECT * FROM users WHERE user_id = :user_id';
$stmt = $connection->prepare($sql);
$stmt->execute([':user_id' => $user_id]);
$user = $stmt->fetch(PDO::FETCH_OBJ);

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$username = $user_email = $user_password = $confirm_password = $user_role = "";
$errors = [];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['username']) && !empty($_POST['username'])) {
        $username = test_input($_POST['username']);
    } else {
        $errors['username_error'] = 'Please enter a valid username';
    }

    if (strlen($username) <= 3) {
        $errors['namelen_error'] = "Choose a username longer then 3 character";
    }

    $user_email = test_input($_POST['user_email']);
    if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $errors['email_error'] = 'Please enter a valid email address';
    }

    if (isset($_POST['user_password']) && !empty($_POST['user_password'])) {
        $user_password = $_POST["user_password"];
    } else {
        $errors['pass_error'] = 'Please enter your password';
    }

    if (isset($_POST['confirm_password']) && !empty($_POST['confirm_password'])) {
        $confirm_password = $_POST["confirm_password"];
    } else {
        $errors['repass_error'] = 'Please reenter your password';
    }

    if ($user_password != $confirm_password) {
        $errors['passmatch_error'] = "Password doesn't match";
    }

    if (strlen($user_password) <= 5) {
        $errors['passlen_error'] = "Choose a password longer then 5 character";
    }

    function userExists($connection, $username, $user_id)
    {
        $userQuery = "SELECT * FROM users WHERE username=:username except select * from users where user_id = :user_id";
        $stmt = $connection->prepare($userQuery);
        $stmt->execute(array(':username' => $username, ':user_id' => $user_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    $nameexists = userExists($connection, $username, $user_id);
    if ($nameexists) {
        $errors['nametaken_error'] = "Sorry... username already taken";
    }

    function emailExists($connection, $user_email, $user_id)
    {
        $userQuery = "SELECT * FROM users WHERE user_email=:user_email except select * from users where user_id = :user_id";
        $stmt = $connection->prepare($userQuery);
        $stmt->execute(array(':user_email' => $user_email, ':user_id' => $user_id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    $emailexists = emailExists($connection, $user_email, $user_id);
    if ($emailexists) {
        $errors['emailtaken_error'] = "Sorry... Email already Exist";
    }

    if (empty($errors)) {
        $sql = 'UPDATE users SET username = :username, user_email = :user_email, user_password = :user_password WHERE user_id = :user_id';
        $stmt = $connection->prepare($sql);
        if ($stmt->execute([':username' => $username, ':user_email' => $user_email, ':user_password' => $user_password, ':user_id' => $user_id])) {
            header("Location: /library-management/user-list.php");
        } else {
            echo 'User Update failed';
        }
    }

}

 } else {
$_SESSION['msg'] = "You must log in as admin or user first";
header("location: /library-management/login.php");
}

?>

<div class="signup-form">
    <form action="" method="post">
		<div class="form-header">
			<h2>Update your profile</h2>
		</div>
        <div class="form-group">
			<label>Username</label>
        	<input value="<?php echo $user->username; ?>" type="text" class="form-control" name="username" >

<span class="text-danger">
<?php
if (isset($errors['namelen_error'])) {
    echo $errors['namelen_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['nametaken_error'])) {
    echo $errors['nametaken_error'];
}
?></span>

        </div>
        <div class="form-group">
			<label>Email Address</label>
        	<input value="<?php echo $user->user_email; ?>" type="email" class="form-control" name="user_email" >

<span class="text-danger">
<?php
if (isset($errors['email_error'])) {
    echo $errors['email_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['emailtaken_error'])) {
    echo $errors['emailtaken_error'];
}
?></span>

        </div>
		<div class="form-group">
			<label>Password</label>
            <input value="<?php echo $user->user_password; ?>" type="password" class="form-control" name="user_password" >

<span class="text-danger">
<?php
if (isset($errors['pass_error'])) {
    echo $errors['pass_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['passlen_error'])) {
    echo $errors['passlen_error'];
}
?></span>

        </div>
		<div class="form-group">
			<label>Confirm Password</label>
            <input value="<?php echo $user->user_password; ?>" type="password" class="form-control" name="confirm_password" >

<span class="text-danger">
<?php
if (isset($errors['repass_error'])) {
    echo $errors['repass_error'];
}
?></span>
<span class="text-danger"><?php
if (isset($errors['passmatch_error'])) {
    echo $errors['passmatch_error'];
}
?></span>

        </div>
		<div class="form-group">
			<button name="submit" type="submit" class="btn btn-primary btn-block btn-lg">Update</button>
		</div>
    </form>
</div>

<?php include 'inc/footer.php';?>