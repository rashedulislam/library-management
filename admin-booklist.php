<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$sql = 'SELECT * FROM books';
$stmt = $connection->prepare($sql);
$stmt->execute();
$books = $stmt->fetchAll(PDO::FETCH_OBJ);
?>

<div class="container">
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<div class="table-responsive">
				<table class="table user-list">
					<thead>
						<tr>
							<th><span>Book Name</span></th>
							<th><span>Author Name</span></th>
							<th><span>Availabe</span></th>
							<th><span>Actions</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($books as $book) {?>

						<tr>
							<td>
								
								<a href="viewbook.php?book_id=<?php echo $book->book_id; ?>" class="user-link"><?php echo $book->book_name; ?></a>
							</td>
							<td>
                                <span><?php echo $book->author_name; ?></span>
							</td>
							<td>
                            <span><?php echo $book->amount; ?></span>
							</td>
							<td style="width: 20%;">
								<a href="updatebooks.php?book_id=<?php echo $book->book_id; ?>" class="table-link">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="deletebooks.php?book_id=<?php echo $book->book_id; ?>" class="table-link danger">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>

<?php include 'inc/footer.php';?>