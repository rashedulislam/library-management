<?php include 'inc/header.php';

if (!isMember()) {
	$_SESSION['msg'] = "You must log in as member first";
	header("location: /library-management/login.php");
}

$id = $_GET['id'];

$sql = 'UPDATE book_user SET request_status = "return" where id = :id ';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':id' => $id])){
header("Location: /library-management/borrowed_book.php");
}