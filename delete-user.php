<?php include 'inc/header.php';

$user_id = $_GET['user_id'];

$sql = 'DELETE FROM users WHERE user_id = :user_id';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':user_id' => $user_id])) {
    header("Location: /library-management/user-list.php");
} else {
    echo 'User Delete Unsuccessful!';
}

