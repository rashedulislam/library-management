<?php include 'inc/header.php';

if(isset($_SESSION['user'])){
	header('location: /library-management/user-profile.php');
}

$errors =[];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = test_input($_POST['username']);
		$user_password = test_input($_POST['user_password']);

		if (empty($username)) {
			array_push($errors, "Username is required");
		}
		if (empty($user_password)) {
			array_push($errors, "Password is required");
		}

		if (count($errors) == 0) {
            $user_password = md5($user_password);
            
            $userQuery = "SELECT * FROM users WHERE username='$username' AND user_password='$user_password' LIMIT 1";
            $stmt = $connection->prepare($userQuery);
            $stmt->execute();
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			

			if ($results) { 
				if ($results['user_role'] == 'admin') {

					$_SESSION['user'] = $results;
					$_SESSION['success']  = "You are now logged in";
					header('location: /library-management/user-list.php');

				}else{
					$_SESSION['user'] = $results;
					$_SESSION['success']  = "You are now logged in";

					header('location: /library-management/user-profile.php');
				}
			}else {
				array_push($errors, "Wrong username/password combination");
			}
		}

    }

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>

<?php 
		if (isset($_SESSION['msg'])) : ?>
			<div class="error success" >
				<h3>
					<?php 
						echo $_SESSION['msg']; 
						unset($_SESSION['msg']);
					?>
				</h3>
			</div>
		<?php endif ?>

<div class="login-form">
  <?php foreach ($errors as $error){
					echo $error .'<br>';
				}?>
    <form action="" method="post">
		<div class="avatar"><i class="material-icons">&#xE7FF;</i></div>
    	<h4 class="modal-title">Login to Your Account</h4>
        <div class="form-group">
            <input name="username" type="text" class="form-control" placeholder="Username">
        </div>
        <div class="form-group">
            <input name="user_password" type="password" class="form-control" placeholder="Password">
        </div>
        <input name="submit" type="submit" class="btn btn-primary btn-block btn-lg" value="Login">
    </form>
    <div class="text-center small">Don't have an account? <a href="register.php">Sign up</a></div>
</div>

<?php include 'inc/footer.php';?>
