<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$sql = 'SELECT * FROM book_user INNER JOIN users ON book_user.user_id = users.user_id INNER JOIN books ON book_user.book_id = books.book_id WHERE request_status = "pending" ';
$stmt = $connection->prepare($sql);
$stmt->execute();
$request_books = $stmt->fetchAll(PDO::FETCH_OBJ);

$sql = 'SELECT * FROM book_user INNER JOIN users ON book_user.user_id = users.user_id INNER JOIN books ON book_user.book_id = books.book_id WHERE request_status = "return" ';
$stmt = $connection->prepare($sql);
$stmt->execute();
$return_books = $stmt->fetchAll(PDO::FETCH_OBJ);


?>

<div class="container">
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<div class="table-responsive">
			<h2>Request Book Table</h2>
				<table class="table user-list">
					<thead>
						<tr>
							<th><span>Book Name</span></th>
							<th><span>Author Name</span></th>
							<th><span>Requested Amount</span></th>
                            <th><span>Requested User</span></th>
                            <th><span>Actions</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($request_books as $book) {?>

						<tr>
							<td>
								<h4><?php echo $book->book_name; ?></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->author_name; ?></span></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->quantity; ?></span></h4>
							</td>
                            <td>
                                <h4><span><?php echo $book->username; ?></span></h4>
							</td>
                            <td>
                            <a href="request_approve.php?id=<?php echo $book->id; ?>" class="table-link">
							<span 
							<i class="fa fa-check-square-o" aria-hidden="true"></i>
							</span>
								</a>
								<a href="request_reject.php?id=<?php echo $book->id; ?>" class="table-link danger">
								<span 
							     class="glyphicon glyphicon-remove" aria-hidden="true">
							    </span>
								</a>
							</td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>

			<hr>

			<div class="table-responsive">
			<h2>Return Book Table</h2>
				<table class="table user-list">
					<thead>
						<tr>
							<th><span>Book Name</span></th>
							<th><span>Author Name</span></th>
							<th><span>Return Amount</span></th>
                            <th><span>Requested User</span></th>
                            <th><span>Actions</span></th>
						</tr>
					</thead>
					<tbody>
                        <?php foreach ($return_books as $book) {?>

						<tr>
							<td>
								<h4><?php echo $book->book_name; ?></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->author_name; ?></span></h4>
							</td>
							<td>
                                <h4><span><?php echo $book->quantity; ?></span></h4>
							</td>
                            <td>
                                <h4><span><?php echo $book->username; ?></span></h4>
							</td>
                            <td>
                            <a href="return_approve.php?id=<?php echo $book->id; ?>" class="table-link">
							<span 
							<i class="fa fa-check-square-o" aria-hidden="true"></i>
							</span>
								</a>
								<a href="return_reject.php?id=<?php echo $book->id; ?>" class="table-link danger">
								<span 
							     class="glyphicon glyphicon-remove" aria-hidden="true">
							    </span>
								</a>
							</td>
                        </tr>
                        <?php }?>
					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>
</div>

<?php include 'inc/footer.php';?>