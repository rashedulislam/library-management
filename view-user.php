<?php include 'inc/header.php';

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$user_id = $_GET['user_id'];

$sql = 'SELECT * FROM users WHERE user_id = :user_id';
$stmt = $connection->prepare($sql);
$stmt->execute(['user_id' => $user_id]);
$user = $stmt->fetch(PDO::FETCH_OBJ);

?>

<div class="container">
	<div class="content">
		<h2>Profile of <?php echo $user-> username ;?></h2>
        <div class="profile_info">
            <img src="https://bootdey.com/img/Content/avatar/avatar1.png"  >
        <div> <br>
            <table class="view-user-table" >
                <tr>
                <th>Username</th>
                <td><strong><?php echo $user->username; ?></strong></td>
                </tr>
                <tr>
                <th>Email Address</th>
                <td><strong><?php echo $user->user_email; ?></strong></td>
                </tr>
                <tr>
                <th>User Role</th>
                <td><strong><?php echo $user->user_role; ?></strong></td>
                </tr>
                <tr>
                <th>Register At</th>
                <td><strong><?php echo $user->created_at; ?></strong></td>
                </tr>
            </table>
        </div>
     </div>
  </div>
  <hr>
  <a href="user-list.php"><button type="button" class="btn btn-default">Return</button></a>
</div>

<?php include 'inc/footer.php';?>