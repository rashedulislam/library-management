<?php include 'inc/header.php';
 
if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in as admin first";
	header("location: /library-management/login.php");
}

$book_id = $_GET['book_id'];

$sql = 'DELETE FROM books WHERE book_id = :book_id';
$stmt = $connection->prepare($sql);
if ($stmt->execute([':book_id' => $book_id])) {
    header("Location: /library-management/admin-booklist.php");
} else {
    echo 'Book Delete Unsuccessful!';
}
